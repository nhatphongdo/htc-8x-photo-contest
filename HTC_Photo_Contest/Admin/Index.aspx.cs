﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using HTC_Photo_Contest;
using System.Text;


namespace HTC_Photo_Contest.Admin
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["pass"] == null || Session["pass"].ToString() != "login")
            {
                Response.Redirect("../Admin/Login.aspx");
            }


            LoadData();
            RadGrid1.DataBind();
        }

        private void LoadData()
        {
            var data = new HTC8X_Photo_ContestEntities();

            var GridData = (from p in data.Photos
                            from c in data.Users
                            where p.UserID == c.ID
                            select new
                                       {
                                           p.ID,
                                           p.PhotoPath,
                                           c.FullName,
                                           c.FacebookName,
                                           c.PhoneNumber,
                                           p.Likes,
                                           p.IsPublished,
                                           p.UploadedDate
                                       });

            RadGrid1.DataSource = GridData.ToList();
        }

        protected void RadGrid1_EditCommand(object sender, GridCommandEventArgs e)
        {
            var data = new HTC8X_Photo_ContestEntities();
            var valueitem = (GridEditableItem)e.Item;
            var cell = valueitem["column6"];
            var strHiddenField = cell.Text;
            int id;
            if (!int.TryParse(strHiddenField.Trim(), out id))
            {
                id = 0;
            }
            var photo = data.Photos.SingleOrDefault(p => p.ID == id);
            if (photo != null)
            {
                if (photo.IsPublished == true)
                {
                    photo.IsPublished = false;
                }
                else
                {
                    photo.IsPublished = true;
                }
                data.SaveChanges();
            }

            Response.Redirect("../Admin/Index.aspx");
        }

        public void ExportOrder()
        {
            var data = new HTC8X_Photo_ContestEntities();

            var GridData = from p in data.Photos
                           from c in data.Users
                           where p.UserID == c.ID
                           select new
                           {
                               p.ID,
                               c.Address,
                               c.CreatedOn,
                               p.PhotoPath,
                               c.FullName,
                               c.FacebookName,
                               c.PhoneNumber,
                               p.Likes,
                               p.IsPublished,
                               p.UploadedDate,
                               c.Email,
                               p.IP
                           };

            var result = new StringBuilder();

            result.AppendLine("<table border='1'>");
            result.AppendLine("<tr>");
            result.AppendLine("<thead>");
            result.AppendLine("<td style='font-weight: bold;'>Họ và tên</td>");
            result.AppendLine("<td style='font-weight: bold;'>Tên facebook</td>");
            result.AppendLine("<td style='font-weight: bold;'>Ngày tạo</td>");
            result.AppendLine("<td style='font-weight: bold;'>Địa chỉ</td>");
            result.AppendLine("<td style='font-weight: bold;'>Địa chỉ email</td>");
            result.AppendLine("<td style='font-weight: bold;'>Số điện thoại</td>");
            result.AppendLine("<td style='font-weight: bold;'>Ngày Upload</td>");
            result.AppendLine("<td style='font-weight: bold;'>Số lượt like</td>");
            result.AppendLine("<td style='font-weight: bold;'>Địa chỉ IP upload ảnh</td>");


            result.AppendLine("</thead>");
            result.AppendLine("</tr>");

            foreach (var row in GridData)
            {
                result.AppendLine("<tr>");
                result.AppendFormat("<td>{0}</td>", row.FullName);
                result.AppendFormat("<td>{0}</td>", row.FacebookName);
                result.AppendFormat("<td>{0}</td>", row.CreatedOn);
                result.AppendFormat("<td>{0}</td>", row.Address);
                result.AppendFormat("<td>{0}</td>", row.Email);
                result.AppendFormat("<td>{0}</td>", row.PhoneNumber);
                result.AppendFormat("<td>{0}</td>", row.UploadedDate);
                result.AppendFormat("<td>{0}</td>", row.Likes);
                result.AppendFormat("<td>{0}</td>", row.IP);
                result.AppendLine("</tr>");
            }

            result.AppendLine("</table>");
            Response.Clear();
            Response.ContentType = "application/ms-excel;charset=utf-8";
            Response.AddHeader("Content-Disposition", "attachment; filename=HTCPhoTo.xls");
            Response.BinaryWrite(System.Text.Encoding.UTF8.GetBytes(result.ToString()));
            Response.End();
        }

        protected void btexport_Click(object sender, EventArgs e)
        {
            ExportOrder();
        }
<<<<<<< HEAD




=======
>>>>>>> 38abc71e34d27d5d44f68b2328ba93fe9eab65ff
    }
}