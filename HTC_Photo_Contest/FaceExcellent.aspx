﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FaceExcellent.aspx.cs" Inherits="HTC_Photo_Contest.FaceExcellent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/jquery.cj_object_scaler.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            selectMenu(3);

            $('.sort-box').text($('.select-box').children('option:selected').text());

            if ($.browser.msie && parseInt($.browser.version, 10) < 9) {
                $('.sort-box').hide();
            }
            $('.select-box').change(function () {
                $(this).prev().text($(this).children('option:selected').text());
            });

            // Align paging
            $('.paging').css("margin-left", (726 - $('.paging').width()) / 2 + "px");

            $('.gallery-page .images-list li .image-box div img').each(function () {
                $(this).cjObjectScaler({
                    method: "fit",
                    fade: 550
                });
            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyPlaceHolder" runat="server">
    <div class="gallery-page" style="padding: 0px 35px;">
        <div class="phone-image"></div>
        <div class="headline">
            <div>
                <span>Seû chia khoaûnh kh</span>
                <span style="width: 13px;">aé</span>
                <span>c</span>
            </div>
            <h2 class="clear">
                <span>"Cuøng nhau th</span>
                <span style="width: 28px;">eå</span>
                <span>hi</span>
                <span style="width: 18px;">eä</span>
                <span>n caù tính"</span>
            </h2>
            <div class="clear">
                ñeå coù cô hoäi sôû höõu giaûi thöôûng cöïc kool<br />
            </div>
        </div>

        <div class="clear"></div>
        <div style="font-family: Arial; color: #7FC719; font-size: 26px; font-weight: normal; text-transform: uppercase; margin-top: 30px; text-align: center;">
            CHÚC MỪNG 8 BẠN DƯỚI ĐÂY BƯỚC TIẾP VÀO VÒNG 2 CỦA CUỘC THI CÙNG HTC
        </div>
        <div>
            <ul class="images-list">
                <%--<asp:Repeater runat="server" ID="ImagesLine1">
                    <ItemTemplate>
                        <li>
                            <a class="image-box" href="javascript:void(0);" onclick="showImage(<%# Eval("ID") %>, '<%# Eval("PhotoPath") %>', '<%# Eval("FullName") %>', <%# Eval("Likes") %>, true)">
                                <div>
                                    <img src="<%# Eval("PhotoPath") %>" alt="" />
                                </div>
                            </a>
                            <div class="title">bởi <strong><%# Eval("FullName") %></strong></div>
                            <div class="like"><%# Eval("Likes") %> Likes</div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>--%>
                <li>
                    <a class="image-box">
                        <div>
                            <img src="http://vietdev.vn/htc_8x_photo_contest/Upload/77_634900711442003832.jpg" alt="" />
                        </div>
                    </a>
                    <div class="title">bởi <strong>nguyen ngoc hao</strong></div>
                    <div class="like">230 Likes</div>
                </li>
                <li>
                    <a class="image-box">
                        <div>
                            <img src="http://vietdev.vn/htc_8x_photo_contest/Upload/336_634908228765359343.jpg" alt="" />
                        </div>
                    </a>
                    <div class="title">bởi <strong>Trương Xuân Đào</strong></div>
                    <div class="like">213 Likes</div>
                </li>
                <li>
                    <a class="image-box">
                        <div>
                            <img src="http://vietdev.vn/htc_8x_photo_contest/Upload/326_634908211979729861.jpg" alt="" />
                        </div>
                    </a>
                    <div class="title">bởi <strong>Huỳnh Thy Đan</strong></div>
                    <div class="like">206 Likes</div>
                </li>
                <li>
                    <a class="image-box">
                        <div>
                            <img src="http://vietdev.vn/htc_8x_photo_contest/Upload/26_634898727444403124.jpg" alt="" />
                        </div>
                    </a>
                    <div class="title">bởi <strong>TRẦN CAO KỲ</strong></div>
                    <div class="like">182 Likes</div>
                </li>
                <li>
                    <a class="image-box">
                        <div>
                            <img src="http://vietdev.vn/htc_8x_photo_contest/Upload/338_634908237352862427.jpg" alt="" />
                        </div>
                    </a>
                    <div class="title">bởi <strong>Nguyễn Tuyết My</strong></div>
                    <div class="like">176 Likes</div>
                </li>
                <li>
                    <a class="image-box">
                        <div>
                            <img src="http://vietdev.vn/htc_8x_photo_contest/Upload/316_634908136846097896.jpg" alt="" />
                        </div>
                    </a>
                    <div class="title">bởi <strong>Nguyễn Hoàng Khải</strong></div>
                    <div class="like">161 Likes</div>
                </li>
                <li>
                    <a class="image-box">
                        <div>
                            <img src="http://vietdev.vn/htc_8x_photo_contest/Upload/127_634902337607892041.jpg" alt="" />
                        </div>
                    </a>
                    <div class="title">bởi <strong>Trần Đức Cường</strong></div>
                    <div class="like">134 Likes</div>
                </li>
                <li>
                    <a class="image-box">
                        <div>
                            <img src="http://vietdev.vn/htc_8x_photo_contest/Upload/396_634909012514167925.jpg" alt="" />
                        </div>
                    </a>
                    <div class="title">bởi <strong>Nguyễn Kim Thùy Linh</strong></div>
                    <div class="like">131 Likes</div>
                </li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</asp:Content>
