﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace HTC_Photo_Contest
{
    public partial class Gallery : System.Web.UI.Page
    {
        public const int ItemsOnPage = 8;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                var id = 0;
                int.TryParse(Request["id"], out id);
                using (var entities = new HTC8X_Photo_ContestEntities())
                {
                    var photo = entities.Photos.FirstOrDefault(p => p.ID == id && p.IsPublished);
                    if (photo != null)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "showImagePopup",
                                                            string.Format(
                                                                "<script type='text/javascript'>showImage({0}, '{1}', '{2}', {3})</script>",
                                                                photo.ID, photo.PhotoPath, photo.User.FullName,
                                                                photo.Likes), false);
                    }
                }
            }

            if (Session["SortBy"] != null && !IsPostBack)
            {
                SortByDropDown.SelectedIndex = int.Parse(Session["SortBy"].ToString());
            }

            // Select data
            using (var entities = new HTC8X_Photo_ContestEntities())
            {
                var page = 1;
                if (!int.TryParse(Request["page"], out page))
                {
                    page = 1;
                }

                var query = entities.Photos.Where(p => p.IsPublished);

                if (!string.IsNullOrEmpty(Search.Text))
                {
                    query = query.Where(
                            p => p.User.FullName.ToLower().Contains(Search.Text.ToLower()) ||
                                 p.User.FacebookName.ToLower().Contains(Search.Text.ToLower()));
                }

                query = SortByDropDown.SelectedIndex == 0 ? query.OrderByDescending(p => p.UploadedDate) : query.OrderByDescending(p => p.Likes);

                query = query.Skip((page - 1) * ItemsOnPage).Take(ItemsOnPage);

                var photos = query.Select(p => new
                                                   {
                                                       ID = p.ID,
                                                       PhotoPath = p.PhotoPath,
                                                       FullName = p.User.FullName,
                                                       Likes = p.Likes
                                                   }).ToList();

                if (photos.Count > 0)
                {
                    ImagesLine1.DataSource = photos.Take(ItemsOnPage / 2);
                }
                else
                {
                    ImagesLine1.DataSource = null;
                }
                ImagesLine1.DataBind();

                if (photos.Count > ItemsOnPage / 2)
                {
                    ImagesLine2.DataSource = photos.Skip(ItemsOnPage / 2).Take(ItemsOnPage / 2);
                }
                else
                {
                    ImagesLine2.DataSource = null;
                }
                ImagesLine2.DataBind();

                var paging = new List<dynamic>();
                query = entities.Photos.Where(p => p.IsPublished == true);

                if (!string.IsNullOrEmpty(Search.Text))
                {
                    query = query.Where(
                            p => p.User.FullName.ToLower().Contains(Search.Text.ToLower()) ||
                                 p.User.FacebookName.ToLower().Contains(Search.Text.ToLower()));
                }

                var count = query.Count();
                var pages = count / ItemsOnPage;
                if (count % ItemsOnPage != 0)
                {
                    pages += 1;
                }
                for (var i = 0; i < pages; i++)
                {
                    paging.Add(new
                                   {
                                       Number = i + 1,
                                       Selected = (i + 1 == page)
                                   });
                }

                PageNumbers.DataSource = paging;
                PageNumbers.DataBind();
            }
        }

        protected void SortByDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Re-sort
            Session["SortBy"] = SortByDropDown.SelectedIndex;
        }
    }
}