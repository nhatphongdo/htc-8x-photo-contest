﻿var W3CDOM = (document.createElement && document.getElementsByTagName);

function initFileUploads() {
    if (!W3CDOM) return;
    var fakeFileUpload = document.createElement('div');
    var fileInput = document.createElement('input');
    fileInput.setAttribute("readonly", "readonly");
    fileInput.setAttribute("placeholder", "Tập tin ảnh");
    fileInput.className = 'textbox upload-input';
    fakeFileUpload.appendChild(fileInput);
    var button = document.createElement('a');
    button.className = 'upload-button';
    button.textContent = "Chọn ảnh";
    fakeFileUpload.appendChild(button);
    var x = document.getElementsByTagName('input');
    for (var i = 0; i < x.length; i++) {
        if (x[i].type != 'file') continue;
        x[i].className = 'hidden';
        var clone = fakeFileUpload.cloneNode(true);
        x[i].parentNode.appendChild(clone);
        x[i].relatedElement = clone.getElementsByTagName('input')[0];
        x[i].relatedElement.onclick = function () {
            this.parentNode.parentNode.getElementsByTagName('input')[0].click();
        };
        clone.getElementsByTagName('a')[0].onclick = function () {
            this.parentNode.parentNode.getElementsByTagName('input')[0].click();
        };
        x[i].onchange = function () {
            this.relatedElement.value = this.value;
        };
        x[i].onselect = function () {
            this.relatedElement.select();
        };
    }
}

function selectMenu(menu) {
    $('.menu li').removeClass('selected');
    $('.menu li:eq(' + menu + ')').addClass('selected');
}

function showPopup() {
    $('#uploadPopup').show();
    $('#uploadPopup').height($('.bg').height());

    $('.bg').blurjs();
    $('body svg').height(0);

    $('#uploadPopup .popup').css("margin-top", (($('#uploadPopup').height() - $('#uploadPopup .popup').height()) / 2) + "px");
    $('#uploadPopup .popup').css("margin-left", (($('#uploadPopup').width() - $('#uploadPopup .popup').width()) / 2) + "px");
}

function closePopup() {
    $('.shadow').hide();
    $('.bg').blurjs('remove');
    $('body svg').remove();
}

function showImage(id, img, name, like, clear) {
    $('#imageViewer .popup img').remove();
    $('#imageViewer .popup .imageContainer').append("<img src='" + img + "' alt='' />");
    $('#imageViewer .popup img').cjObjectScaler({
        method: 'fit',
        fade: 550
    });

    $('#imageViewer .popup #imageTitle').text(name);
    $('#imageViewer .popup #likeCount').text(like);

    $('#imageViewer .popup .share').attr("image_id", id);
    $('#imageViewer .popup .share').attr("image_link", img);

    $('#imageViewer #ImageID').val(id);

    if (clear === true) {
        $('#imageViewer .popup div:last').html("");
    }

    $('#imageViewer').show();
    $('#imageViewer').height($('.bg').height());

    $('.bg').blurjs();
    $('body svg').height(0);

    $('#imageViewer .popup').css("margin-top", (($('#imageViewer').height() - $('#imageViewer .popup').height()) / 2) + "px");
    $('#imageViewer .popup').css("margin-left", (($('#imageViewer').width() - $('#imageViewer .popup').width()) / 2) + "px");
}
