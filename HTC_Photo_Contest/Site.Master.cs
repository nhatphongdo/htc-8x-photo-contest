﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Collections;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace HTC_Photo_Contest
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        private string facebookLink = "http://www.facebook.com/htcvietnam/app_106659269501933?app_data=id%3D";

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var payload = Request.Form["signed_request"].Split('.')[1];
                var encoding = new UTF8Encoding();
                var decodedJson = payload.Replace("=", string.Empty).Replace('-', '+').Replace('_', '/');
                var base64JsonArray =
                    Convert.FromBase64String(decodedJson.PadRight(decodedJson.Length + (4 - decodedJson.Length % 4) % 4, '='));
                var json = encoding.GetString(base64JsonArray);

                var o = JObject.Parse(json);

                if (!string.IsNullOrEmpty(o.SelectToken("app_data").Value<string>()))
                {
                    var parts = o.SelectToken("app_data").Value<string>().Split(new char[]
                                                                                    {
                                                                                        '='
                                                                                    },
                                                                                StringSplitOptions.RemoveEmptyEntries);

                    if (parts.Length > 1)
                    {
                        Response.Redirect(string.Format("Gallery.aspx?fid={0}&id={1}", o.SelectToken("user_id").Value<string>(), parts[1]));
                    }
                }
            }
            catch (Exception exc)
            {
            }

            // Check uploaded photo
            if (!string.IsNullOrEmpty(Request["fid"]))
            {
                using (var entities = new HTC8X_Photo_ContestEntities())
                {
                    var facebookID = Request["fid"];
                    var user = entities.Users.FirstOrDefault(u => u.FacebookID == facebookID);
                    if (user != null)
                    {
                        var photo = user.Photos.FirstOrDefault();
                        if (photo != null)
                        {
                            PhotoLink.Text = facebookLink + photo.ID;
                            ShareButton.Text = string.Format("<a class='button share' image_id={0} image_link={1}>chia sẻ lên facebook<span class='arrow'></span></a>", photo.ID, photo.PhotoPath);

                            UploadFileViews.SetActiveView(ThanksPopup);
                        }
                    }
                }
            }
        }

        protected void SubmitPhoto_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "showUploadPopup", "<script type='text/javascript'>showPopup();</script>", false);

            // Validate
            if (string.IsNullOrEmpty(FacebookID.Value))
            {
                return;
            }

            if (string.IsNullOrEmpty(FullName.Text))
            {
                ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Bạn chưa nhập Họ và Tên</label>";
                return;
            }

            if (string.IsNullOrEmpty(Email.Text))
            {
                ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Bạn chưa nhập Địa chỉ Email</label>";
                return;
            }

            var regexEmail = new System.Text.RegularExpressions.Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            if (!regexEmail.IsMatch(Email.Text))
            {
                ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Địa chỉ Email không hợp lệ</label>";
                return;
            }

            if (string.IsNullOrEmpty(PhoneNumber.Text))
            {
                ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Bạn chưa nhập Số điện thoại</label>";
                return;
            }

            var regexPhoneNumber = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
            if (!regexPhoneNumber.IsMatch(PhoneNumber.Text) || PhoneNumber.Text.Length < 6 || PhoneNumber.Text.Length > 15)
            {
                ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Số điện thoại không hợp lệ</label>";
                return;
            }

            if (!FileUpload.HasFile)
            {
                ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Bạn phải chọn tập tin ảnh để tải lên</label>";
                return;
            }

            if (!FileUpload.FileName.ToLower().EndsWith(".jpg") && !FileUpload.FileName.ToLower().EndsWith(".jpeg") && !FileUpload.FileName.ToLower().EndsWith(".png"))
            {
                ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Tập tin ảnh phải có định dạng JPEG hoặc PNG</label>";
                return;
            }

            if (FileUpload.PostedFile.ContentLength > 1024768)
            {
                ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Tập tin ảnh phải có dung lượng không quá 1 MB</label>";
                return;
            }

            if (!Aggrement.Checked)
            {
                ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Bạn phải Đồng ý với điều khoản của chương trình</label>";
                return;
            }

            using (var entities = new HTC8X_Photo_ContestEntities())
            {
                var user = entities.Users.FirstOrDefault(u => u.FacebookID == FacebookID.Value);
                if (user == null)
                {
                    user = new User
                               {
                                   CreatedOn = DateTime.Now,
                                   Email = Email.Text,
                                   FacebookID = FacebookID.Value,
                                   FacebookName = FacebookName.Value,
                                   FullName = FullName.Text,
                                   IP = Utility.GetIPAddress(),
                                   PhoneNumber = PhoneNumber.Text
                               };
                    entities.Users.Add(user);
                    entities.SaveChanges();
                }

                var photo = entities.Photos.FirstOrDefault(p => p.UserID == user.ID);
                if (photo != null)
                {
                    // Already upload photo
                    ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Bạn đã tham gia tải ảnh rồi</label>";
                    return;
                }

                var fileName = string.Format("Upload/{0}_{1}.jpg", user.ID, DateTime.Now.Ticks);
                try
                {
                    var bitmap = Bitmap.FromStream(FileUpload.PostedFile.InputStream);

                    var propertyItems = bitmap.PropertyItems;
                    foreach (var property in propertyItems)
                    {
                        if (property.Id == 274)
                        {
                            // Orientation
                            switch (property.Value[0])
                            {
                                case 1:
                                    // Keep as original
                                    break;
                                case 2:
                                    // Flip X
                                    bitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
                                    break;
                                case 3:
                                    // Rotate right 90 degree
                                    bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                    break;
                                case 4:
                                    // Flip Y
                                    bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
                                    break;
                                case 5:
                                    // Rotate right 90 degree and flip X
                                    bitmap.RotateFlip(RotateFlipType.Rotate90FlipX);
                                    break;
                                case 6:
                                    // Rotate right 90 degree
                                    bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                    break;
                                case 7:
                                    // Rotate left 90 degree and flip X
                                    bitmap.RotateFlip(RotateFlipType.Rotate270FlipX);
                                    break;
                                case 8:
                                    // Rotate left 90 degree
                                    bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                    break;
                            }
                        }
                    }

                    var width = bitmap.Width;
                    var height = bitmap.Height;

                    if (width > 500)
                    {
                        height = height * 500 / width;
                        width = 500;
                    }

                    if (height > 400)
                    {
                        width = width * 400 / height;
                        height = 400;
                    }

                    var resizedBitmap = new Bitmap(bitmap, width, height);
                    resizedBitmap.Save(Server.MapPath(fileName), System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                catch (Exception exc)
                {
                    ErrorMessage.Text = "<label style='padding-bottom: 20px;'>Xảy ra lỗi trong quá trình tải ảnh lên. Bạn hãy tải lại trang và thử lại.</label>";
                    return;
                }

                photo = new Photo()
                            {
                                IP = Utility.GetIPAddress(),
                                Likes = 0,
                                PhotoPath = fileName,
                                UploadedDate = DateTime.Now,
                                UserID = user.ID,
                                IsPublished = true
                            };
                entities.Photos.Add(photo);
                entities.SaveChanges();

                PhotoLink.Text = facebookLink + photo.ID;

                ShareButton.Text = string.Format("<a class='button share' image_id={0} image_link={1}>chia sẻ lên facebook<span class='arrow'></span></a>", photo.ID, photo.PhotoPath);

                UploadFileViews.SetActiveView(ThanksPopup);
            }
        }

        protected void LikeButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ImageID.Value) || string.IsNullOrEmpty(FacebookID.Value))
            {
                return;
            }

            using (var entities = new HTC8X_Photo_ContestEntities())
            {
                var id = 0;
                int.TryParse(ImageID.Value, out id);

                var photo = entities.Photos.FirstOrDefault(p => p.ID == id);
                if (photo == null)
                {
                    return;
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "showImageViewer",
                    string.Format("<script type='text/javascript'>showImage({0}, '{1}', '{2}', {3})</script>", photo.ID, photo.PhotoPath, photo.User.FullName, photo.Likes), false);

                var like = entities.Likes.FirstOrDefault(l => l.PhotoID == photo.ID && l.FacebookID == FacebookID.Value);
                if (like != null)
                {
                    LikeError.Text = "<div class='error'>Bạn đã Like bức ảnh này rồi</div>";
                    return;
                }

                photo.Likes += 1;

                like = new Like()
                           {
                               FacebookID = FacebookID.Value,
                               CreatedDate = DateTime.Now,
                               IP = Utility.GetIPAddress(),
                               PhotoID = photo.ID
                           };
                entities.Likes.Add(like);

                LikeError.Text = "<div class='success'>Bạn đã Like bức ảnh thành công</div>";
                entities.SaveChanges();
            }
        }
    }
}