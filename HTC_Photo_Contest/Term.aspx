﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Term.aspx.cs" Inherits="HTC_Photo_Contest.Term" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Content/jquery.jscrollpane.css" rel="stylesheet" />
    <script src="Scripts/mwheelIntent.js"></script>
    <script src="Scripts/jquery.mousewheel.js"></script>
    <script src="Scripts/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            selectMenu(4);

            $('.content').bind(
                'jsp-initialised',
                function (event, isScrollable) {
                    if (isScrollable) {
                        $('.jspDrag').append("<div class='jspLines'><div class='jspLine' style='margin-top: " + ($('.jspDrag').height() / 2 - 3) + "px;'></div><div class='jspLine'></div><div class='jspLine'></div></div>");
                    }
                }
            ).jScrollPane();
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyPlaceHolder" runat="server">
    <div class="term-page">
        <div class="phone-image"></div>
        <div class="headline">
            <span>Th</span>
            <span style="width: 30px;">eå</span>
            <span>l</span>
            <span style="width: 30px;">eä</span>
            <span>tham döï</span>
        </div>
        <h1 class="title">CUỘC THI ẢNH CHỦ ĐỀ “CÙNG NHAU THỂ HIỆN CÁ TÍNH”</h1>
        <div class="content">
            <h2>I- Đối tượng dự thi:</h2>
            <ul>
                <li>Tất cả những bạn yêu thích chụp ảnh.</li>
                <li>Công dân Việt Nam trên 18 tuổi.</li>
                <li>Chương trình chỉ diễn ra tại ba địa điểm lớn: Thành Phố Hồ Chí Minh và Hà Nội & Đà Nẵng</li>
            </ul>
            <h2>II- Thể lệ tham dự:</h2>
            <p>
                Cuộc thi được chia làm 02 vòng:
            </p>
            <ol>
                <li>Vòng 1: “Nào ta cùng Pose” :
                <ul>
                    <li>Thời gian: Bắt đầu từ 12:00 PM ngày 29-11-2012 và kết thúc vào 12:00 PM ngày 12-12-2012</li>
                    <li>Qui cách tham dự cuộc thi:
                        <ul>
                            <li>Người tham dự phải LIKE trang HTC Việt Nam Facebook tại địa chỉ: <a target="_blank" href="http://www.facebook.com/htcvietnam/app_106659269501933">http://www.facebook.com/htcvietnam/app_106659269501933</a></li>
                            <li>Người tham dự tải duy nhất 01 ảnh dự thi vào trang “Bộ Sưu Tập” </li>
                        </ul>
                    </li>
                    <li>Qui cách ảnh dự thi:
                        <ul>
                            <li>Ảnh dự thi phải là ảnh có sự hiện diện của thí sinh</li>
                            <li>Ảnh phải được chụp bằng Camera trước của sản phẩm điện thoại thí sinh đang sử dụng</li>
                            <li>Ảnh phải có ít nhất 2 người trong hình trở lên</li>
                            <li>Ảnh hợp lệ là ảnh thể hiện được việc thí sinh tự chụp bằng Camera trước của sản phẩm điện thoại</li>
                            <li>Dung lượng ảnh không quá 1MB và phải là file JPEG hoặc PNG</li>
                            <li>Top 8 ảnh dự thi được bình chọn yêu thích nhiều nhất sẽ được chọn vào vòng thi thứ hai: 03 ảnh tại TPHCM ; 03 ảnh tại Hà Nội, và 02 ảnh tại Đà Nẵng</li>
                            <li>Thí sinh được quyền kêu gọi bình chọn ảnh dự thi bằng cách chia sẻ ảnh dự thi lên Wall của Facebook cá nhân để bạn bè, người thân ủng hộ bình chọn.</li>
                            <li>Thí sinh phải gửi ảnh gốc về Ban Tổ Chức để xét duyệt tính hợp lệ của ảnh dự thi khi được yêu cầu</li>
                        </ul>
                    </li>
                    <li>Công bố kết quả vòng 1:
                        <ul>
                            <li>Việc bình chọn cho ảnh dự thi sẽ chấm dứt hiệu lực vào lúc 12:00 PM ngày 12-12-2012. Ban Tổ Chức sẽ tạm đóng vòng 1 để xem xét các kết quả bình chọn cuối cùng.</li>
                            <li>Kết quả cuộc thi sẽ được công bố vào lúc 12:00 PM ngày 13-12-2012 trên trang HTC Việt Nam Facebook</li>
                        </ul>
                    </li>
                </ul>
                </li>
                <li>Vòng 2:  “Khoảnh khắc Giáng Sinh Trắng”
                <ul>
                    <li>Thời gian: từ 12:00 PM ngày 14-12-2012 cho đến 12:00 PM ngày 23-12-2012.</li>
                    <li>Qui cách tham dự cuộc thi:
                        <ul>
                            <li>08 thí sinh đạt giải vòng 1 sẽ đến gặp đại diện của Ban Tổ Chức để nhận sản phẩm HTC Windows Phone 8X thực hiện “Khoảnh khắc Giáng Sinh Trắng”</li>
                            <li>Mỗi thí sinh phải dùng điện thoại HTC Windows Phone 8X để chụp lại “khoảnh khắc Giáng Sinh Trắng” và tải lên trang “Khoảnh Khắc Giáng Sinh trắng”</li>
                        </ul>
                    </li>
                    <li>Qui cách ảnh dự thi:
                        <ul>
                            <li>Ảnh phải được chụp bằng Camera trước của điện thoại HTC Windows Phone 8X</li>
                            <li>Ảnh phải có ít nhất 5 người xuất hiện trong ảnh và được chụp bằng Camera trước của điện thoại HTC Windows Phone 8X</li>
                            <li>Ảnh chụp phải thể hiện được cá tính và sự sáng tạo của thí sinh liên quan đến dịp lễ Giáng Sinh 2012</li>
                            <li>Dung lượng ảnh tải lên không quá 1MB và phải là định dạng JPEG hoặc PNG</li>
                            <li>Thí sinh phải gửi ảnh gốc về Ban Tổ Chức để xét duyệt tính hợp lệ của ảnh dự thi khi được yêu cầu</li>
                        </ul>
                    </li>
                    <li>Công bố kết quả vòng 2:
                        <ul>
                            <li>Việc bình chọn cho ảnh dự thi sẽ chấm dứt hiệu lực vào lúc 12:00 PM ngày 23-12-2012. Ban Tổ Chức sẽ tạm đóng vòng 2 để xem xét các kết quả bình chọn cuối cùng.</li>
                            <li>Kết quả cuộc thi sẽ được công bố vào lúc 21:00 PM ngày 23-12-2012 trên trang HTC Việt Nam Facebook</li>
                        </ul>
                    </li>
                    <li>Cơ cấu chấm giải vòng 2: 
                        <ul>
                            <li>Thí sinh được quyền kêu gọi bình chọn ảnh dự thi bằng cách chia sẻ ảnh dự thi lên Wall của Facebook cá nhân để bạn bè, người thân ủng hộ bình chọn.</li>
                            <li>05 trong số 08 ảnh dự thi sở hữu lượt bình chọn Yêu Thích nhiều nhất sẽ được Ban Tổ Chức chọn và chấm điểm theo tiêu chí sau để nhận giải chung cuộc và giải khuyến khích:
                                <ul>
                                    <li>Ảnh chụp đăng đúng chủ đề và hợp lệ: thang điểm từ 0 đến 10</li>
                                    <li>Ảnh chụp có yếu tố sáng tạo và độc đáo: thang điểm từ 0 đến 10</li>
                                    <li>Ảnh chụp thể hiện được cá tính của những bạn tham gia chụp: thang điểm từ 0 đến 10</li>
                                    <li>Ảnh rõ nét, rõ mặt những người trong ảnh, ánh sáng đầy đủ: thang điểm từ 0 – 10</li>
                                    <li>Ảnh có càng nhiều người và đáp ứng được đầy đủ những yếu tố trên sẽ được cộng thêm điểm ưu tiên: 5 điểm. </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                </li>
            </ol>
            <h2>V- Cơ cấu giải thưởng: </h2>
            <ul>
                <li>Giải thưởng chung cuộc cho 03 thí sinh sở hữu 03 ảnh cao điểm nhất do Ban Giám Khảo chấm như sau:
                    <ul>
                        <li>01 Giải nhất: sản phẩm HTC Windows Phone 8X</li>
                        <li>02 Giải nhì: sản phẩm HTC Windows Phone 8S</li>
                    </ul>
                </li>
                <li>Giải khuyến khích: 02 tai nghe Beats Solo</li>
            </ul>
            <h2>VI- Quy định chung: </h2>
            <ul>
                <li>Các ảnh tham gia dự thi phải là tác phẩm chưa được đăng tải/công bố trên bất kỳ phương tiện thông tin đại chúng nào ,chưa từng tham gia cuộc thi nào, và không thuộc bất kì nguồn sở hữu nào.</li>
                <li>BTC sẽ không chịu trách nhiệm về việc tranh chấp tác quyền và tính pháp lý của tác phẩm. Người dự thi chịu mọi trách nhiệm liên quan đến vấn đề tác quyền của ảnh dự thi. </li>
                <li>BTC được sử dụng ảnh cho các mục đích quảng bá và truyền thông cho cuộc thi. Ảnh sử dụng trong các mục đích quảng cáo tiếp thị sẽ được BTC liên lạc và thương thảo bản quyền sử dụng ảnh cùng tác giả. </li>
                <li>Ảnh vi phạm thể lệ cuộc thi hay thuần phong mỹ tục Việt Nam sẽ bị loại bỏ mà không cần thông báo trước. </li>
                <li>Nếu có bằng chứng hiển nhiên người chơi gian lận và sao chép tác phẩm thì kết quả của người thắng cuộc sẽ bị tước bỏ.</li>
                <li>Nếu phát hiện tác phẩm trúng giải vi phạm thể lệ cuộc thi hoặc đến 24 giờ trước khi trao giải mà BTC vẫn chưa liên lạc được với người đoạt giải, BTC có quyền rút lại giải để trao cho người kế tiếp.  </li>
                <li>BTC sẽ yêu cầu thành viên cung cấp ảnh gốc với thông tin chi tiết cho tác phẩm đọat giải cho mục đích triển lãm. Nếu sau 48 giờ mà thành viên không cung cấp thông tin theo yêu cầu của BTC thì giải thưởng cho tác phẩm đọat giải sẽ bị tước bỏ và giải thưởng sẽ được chuyển cho các tác phẩm tiếp theo.</li>
                <li>BTC có quyền hủy kết quả giải thưởng trong trường hợp có  tranh chấp và phần lỗi thuộc về người đoạt giải. </li>
                <li>Nếu có một vấn đề phát sinh trước trong hoặc sau cuộc thi, mà vấn đề này nằm ngoài quy định đang có, thì BTC sẽ giữ toàn quyền thảo luận để đưa ra quyết định xử lý vấn đề phát sinh đó.</li>
            </ul>
        </div>
    </div>
</asp:Content>
