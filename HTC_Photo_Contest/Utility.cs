﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTC_Photo_Contest
{
    public static class Utility
    {
        public static string GetIPAddress()
        {
            var context = System.Web.HttpContext.Current;
            var sIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(sIPAddress))
            {
                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                var ipArray = sIPAddress.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                return ipArray[0];
            }
        }
    }
}