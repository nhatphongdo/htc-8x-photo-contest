﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="HTC_Photo_Contest.Admin.Index" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>HTC Photo Contest</title>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Abel|Satisfy' rel='stylesheet' type='text/css' />
    <link href="../Content/default.css" rel="stylesheet" type="text/css" media="all" />
    <!--[if IE 6]>
    <link href="default_ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="ScriptManager1" runat="server" EnableTheming="True">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js"></asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js"></asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js"></asp:ScriptReference>
            </Scripts>
        </telerik:RadScriptManager>
        <div id="header-wrapper">
            <div id="header">
                <div id="logo">
                    <h1><a href="#">HTC Photo Contest</a></h1>
                </div>
                <div id="menu">
                    <ul>
<<<<<<< HEAD
                        <li>Homepage</li>
=======
                        <li><a>Homepage</a></li>
>>>>>>> 38abc71e34d27d5d44f68b2328ba93fe9eab65ff
                        <%--<li><a href="#" accesskey="2" title="">Our Clients</a></li>
                        <li><a href="#" accesskey="3" title="">About Us</a></li>
                        <li><a href="#" accesskey="4" title="">Careers</a></li>
                        <li><a href="#" accesskey="5" title="">Contact Us</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>
        <div id="footer-wrapper">
            <div id="footer-content">
                <div class="wrap-grid">
                    <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
                    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
                    </telerik:RadAjaxManager>
                    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
                    </telerik:RadAjaxLoadingPanel>
                    <asp:Button ID="btexport" runat="server" class="link-style" Text="Export" OnClick="btexport_Click" />
                    <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" GridLines="None" OnEditCommand="RadGrid1_EditCommand">
                        <MasterTableView>
                            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>

                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>

                            <Columns>
                                <telerik:GridMaskedColumn DataField="ID" Display="False" DisplayMask="" FilterControlAltText="Filter column6 column" Mask="" UniqueName="column6">
                                </telerik:GridMaskedColumn>
                                <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter column column" HeaderText="Họ Tên" UniqueName="column" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FacebookName" FilterControlAltText="Filter column1 column" HeaderText="Facebook Name" UniqueName="column1" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="PhotoPath" FilterControlAltText="Filter column2 column" HeaderText="Photo" UniqueName="column2" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"> 
                                    <ItemTemplate>
                                        <asp:Image ID="column2" runat="server" CssClass="images-grid" ImageUrl='<%# "/htc_8x_photo_contest/"+DataBinder.Eval(Container, "DataItem.PhotoPath") %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Likes" FilterControlAltText="Filter column3 column" HeaderText="Likes" UniqueName="column3" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="True">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="UploadedDate" FilterControlAltText="Filter column4 column" HeaderText="Ngày Up ảnh" UniqueName="column4" DataType="System.DateTime" CurrentFilterFunction="GreaterThan" AutoPostBackOnFilter="True">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="IsPublished" FilterControlAltText="Filter column5 column" HeaderText="Published" UniqueName="column5">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridEditCommandColumn FilterControlAltText="Filter EditCommandColumn column" HeaderText="Publish">
                                </telerik:GridEditCommandColumn>
                            </Columns>

                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                            </EditFormSettings>
                        </MasterTableView>

                        <FilterMenu EnableImageSprites="False"></FilterMenu>

                        <GroupingSettings CaseSensitive="False"></GroupingSettings>                        
                    </telerik:RadGrid>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div id="footer">
            <p>Copyright (c) 2012 <a href="http://vietdev.vn/" style="text-decoration: none;">Vietdev.vn</a>. All rights reserved.</p>
        </div>
    </form>
</body>
</html>

shiny vietnam
hang.le@shinyvietnam.com
