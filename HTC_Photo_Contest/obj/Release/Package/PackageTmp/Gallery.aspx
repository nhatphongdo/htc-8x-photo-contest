﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="HTC_Photo_Contest.Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/jquery.cj_object_scaler.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            selectMenu(1);

            $('.sort-box').text($('.select-box').children('option:selected').text());

            if ($.browser.msie && parseInt($.browser.version, 10) < 9) {
                $('.sort-box').hide();
            }
            $('.select-box').change(function () {
                $(this).prev().text($(this).children('option:selected').text());
            });

            // Align paging
            $('.paging').css("margin-left", (726 - $('.paging').width()) / 2 + "px");

            $('.gallery-page .images-list li .image-box div img').each(function () {
                $(this).cjObjectScaler({
                    method: "fit",
                    fade: 550
                });
            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyPlaceHolder" runat="server">
    <div class="gallery-page">
        <div class="phone-image"></div>
        <div class="headline">
            <div>
                <span>Seû chia khoaûnh kh</span>
                <span style="width: 13px;">aé</span>
                <span>c</span>
            </div>
            <h2 class="clear">
                <span>"Cuøng nhau th</span>
                <span style="width: 28px;">eå</span>
                <span>hi</span>
                <span style="width: 18px;">eä</span>
                <span>n caù tính"</span>
            </h2>
            <div class="clear">
                ñeå coù cô hoäi sôû höõu giaûi thöôûng cöïc kool<br />
            </div>
            <a class="button" style="margin-top: 5px;" href="javascript:void(0);" onclick="showPopup('upload');">Tải ảnh ngay<span class="arrow"></span></a>
        </div>
        <div class="clear" style="padding-top: 20px;">
            <div class="sort-region">
                <label>Sắp xếp theo</label>
                <div class="sort-box"></div>
                <asp:DropDownList runat="server" ID="SortByDropDown" CssClass="select-box" AutoPostBack="True" OnSelectedIndexChanged="SortByDropDown_SelectedIndexChanged">
                    <Items>
                        <asp:ListItem Text="Mới nhất" Value="Newest" />
                        <asp:ListItem Text="Yêu thích nhất" Value="Most like" />
                    </Items>
                </asp:DropDownList>
                <span class="arrow"></span>
            </div>
            <asp:TextBox runat="server" ID="Search" CssClass="search-box" AutoPostBack="True" Placeholder="Tìm theo tên"></asp:TextBox>
        </div>
        <div class="clear"></div>
        <div>
            <ul class="images-list">
                <asp:Repeater runat="server" ID="ImagesLine1">
                    <ItemTemplate>
                        <li>
                            <a class="image-box" href="javascript:void(0);" onclick="showImage(<%# Eval("ID") %>, '<%# Eval("PhotoPath") %>', '<%# Eval("FullName") %>', <%# Eval("Likes") %>, true)">
                                <div>
                                    <img src="<%# Eval("PhotoPath") %>" alt="" />
                                </div>
                            </a>
                            <div class="title">bởi <strong><%# Eval("FullName") %></strong></div>
                            <div class="like"><%# Eval("Likes") %> Likes</div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
            <div class="clear"></div>
            <ul class="images-list">
                <asp:Repeater runat="server" ID="ImagesLine2">
                    <ItemTemplate>
                        <li>
                            <a class="image-box" href="javascript:void(0);" onclick="showImage(<%# Eval("ID") %>, '<%# Eval("PhotoPath") %>', '<%# Eval("FullName") %>', <%# Eval("Likes") %>, true)">
                                <div>
                                    <img src="<%# Eval("PhotoPath") %>" alt="" />
                                </div>
                            </a>
                            <div class="title">bởi <strong><%# Eval("FullName") %></strong></div>
                            <div class="like"><%# Eval("Likes") %> Likes</div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
            <div class="clear"></div>
            <div class="paging">
                <asp:Repeater runat="server" ID="PageNumbers">
                    <ItemTemplate>
                        <a <%# (bool)Eval("Selected") ? "class='selected'" : "" %> href="Gallery.aspx?page=<%# Eval("Number") %><%= string.IsNullOrEmpty(Request["fid"]) ? "" : ("&fid=" + Request["fid"]) %>"><%# Eval("Number") %></a>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
